package main

type Guide struct {
	Id         int    `json:"id"`
	Title      string `json:"title"`
	Url        string `json:"url"`
	Mode       string `json:"mode"`
	Tag        string `json:"tag"`
	Script     string `json:"script"`
	Created_at string `json:"created_at"`
	Upvote     string `json:"upvote"`
	Downvote   string `json:"downvote"`
}

// method getter
