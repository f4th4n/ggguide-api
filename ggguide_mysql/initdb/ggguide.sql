-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: ggguide_mysql
-- Generation Time: Aug 05, 2018 at 01:49 PM
-- Server version: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ggguide`
--

-- --------------------------------------------------------

--
-- Table structure for table `guides`
--

CREATE TABLE `guides` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `mode` enum('beginner','intermediate','advanced','tool') NOT NULL DEFAULT 'beginner',
  `tag` varchar(64) NOT NULL DEFAULT 'unknown',
  `script` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upvote` int(8) UNSIGNED NOT NULL DEFAULT '0',
  `downvote` int(8) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guides`
--

INSERT INTO `guides` (`id`, `title`, `url`, `mode`, `tag`, `script`, `created_at`, `upvote`, `downvote`) VALUES
(1, 'List of Demons I', 'https://fusion.dx2wiki.com/', 'tool', 'dx2', '', '2018-07-30 10:57:32', 0, 0),
(2, 'List of Demons II', 'https://aqiu384.github.io/megaten-fusion-tool/dx2/demons', 'tool', 'dx2', '', '2018-07-30 10:57:57', 0, 0),
(3, 'Demon Compendium', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/demons', 'intermediate', 'dx2', '', '2018-07-30 11:24:41', 0, 0),
(4, 'Skills', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/skill-database', 'intermediate', 'dx2', '', '2018-07-30 11:25:03', 0, 0),
(5, 'Leveling', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_leveling', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_leveling\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:25:57', 0, 0),
(6, 'Equipping Brands', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_brands', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_brands\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:26:43', 0, 0),
(7, 'Transferring Skills', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_skill_transfer', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_skill_transfer\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:26:57', 0, 0),
(8, 'Awakening', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_awakening', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_awakening\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:27:12', 0, 0),
(9, 'Evolving', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_evolution', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_evolution\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:27:25', 0, 0),
(10, 'Liberators', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/liberators', 'advanced', 'dx2', '', '2018-07-30 11:28:30', 0, 0),
(11, 'Shops', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_shops', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_shops\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:29:22', 0, 0),
(12, 'Story', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/story', 'advanced', 'dx2', '', '2018-07-30 11:29:47', 0, 0),
(13, 'Aura Gate', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/aura-gate', 'advanced', 'dx2', '', '2018-07-30 11:30:17', 0, 0),
(14, 'Brand of Sin', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/brand-of-sin', 'advanced', 'dx2', '', '2018-07-30 11:30:29', 0, 0),
(15, 'Strange Signal', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/strange-signal', 'advanced', 'dx2', '', '2018-07-30 11:30:44', 0, 0),
(16, 'Eclipse', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/eclipse', 'advanced', 'dx2', '', '2018-07-30 11:30:54', 0, 0),
(17, 'Duel', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/d2-duel', 'beginner', 'dx2', '', '2018-07-30 11:31:03', 0, 0),
(18, 'Hell\'s Park', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/hells-park', 'intermediate', 'dx2', '', '2018-07-30 11:31:14', 0, 0),
(19, 'Tier List', 'https://www.reddit.com/r/Dx2SMTLiberation/wiki/index#wiki_tier_list', 'beginner', 'dx2', 'var interval = setInterval(function() { 	var elmnt = document.getElementById(\"wiki_tier_list\"); 	if(elmnt == null) return;  	elmnt.scrollIntoView(); 	window.scrollBy(0, -53); 	clearInterval(interval) }, 50)', '2018-07-30 11:31:38', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guides`
--
ALTER TABLE `guides`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guides`
--
ALTER TABLE `guides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
