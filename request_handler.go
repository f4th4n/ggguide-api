package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type RequestHandler struct {
	db *sql.DB
}

func (requestHandler *RequestHandler) Ping(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "pong")
}

/*
	/guides/:game_tag/:mode/:limit
	e.g /guides/dx2/intermediate/3
	e.g http://localhost:1234/guides/dx2/intermediate/3
*/
func (requestHandler *RequestHandler) GetGuides(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	defaultLimit := "5"

	gameTag := ps.ByName("game_tag")
	limit := ps.ByName("limit")
	mode := ps.ByName("mode")

	if limit == "" {
		limit = defaultLimit
	}

	w.Header().Set("Content-Type", "application/json")

	query := "SELECT * FROM guides WHERE tag=? AND mode=? ORDER BY id DESC LIMIT ?"
	fmt.Println(query)
	rows, _ := requestHandler.db.Query(query, gameTag, mode, limit)

	guides := make([]Guide, 0, 5)
	for rows.Next() {
		guide := Guide{}
		rows.Scan(
			&guide.Id,
			&guide.Title,
			&guide.Url,
			&guide.Mode,
			&guide.Tag,
			&guide.Script,
			&guide.Created_at,
			&guide.Upvote,
			&guide.Downvote)

		guides = append(guides, guide)
	}

	guidesJsonString, _ := json.Marshal(guides)
	fmt.Fprint(w, string(guidesJsonString))
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "game_tag: %s\n", ps.ByName("game_tag"))
}
