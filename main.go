package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
)

func main() {
	// start mysql
	// mysql connection example: "user:password@tcp(localhost:5555)/dbname"
	db, err := sql.Open("mysql", GG_MYSQL_CONN)
	if err != nil {
		log.Fatal(err)
	}

	requestHandler := RequestHandler{db: db}

	router := httprouter.New()
	router.GET("/", requestHandler.Ping)
	router.GET("/guides/:game_tag/:mode/:limit", requestHandler.GetGuides)

	log.Fatal(http.ListenAndServe(":1234", router))
}
