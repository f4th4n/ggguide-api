FROM golang:1.11beta2-alpine3.8

RUN mkdir -p /app
WORKDIR /app

COPY *.go /app/

RUN apk add git

RUN go get github.com/pilu/fresh
RUN go get github.com/julienschmidt/httprouter
RUN go get github.com/go-sql-driver/mysql

CMD ["fresh"]